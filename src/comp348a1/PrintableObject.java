/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comp348a1;

/**
 *
 * @author Jasmar
 */
public abstract class PrintableObject implements NamedObject, Printable {
    @Override
    public String toString(){
        return this.getName();
    }
    @Override
    public void print(){
        System.out.println(this);
    }
}
