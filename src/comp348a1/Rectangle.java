/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comp348a1;

/**
 *
 * @author Jasmar
 */
public class Rectangle extends PrintableObject implements Shape{
    private double height;
    private double width;
    
    public double getHeight(){
        return this.height;
    }
    public void setHeight(double height){
        this.height = height;
    }
    
    public double getWidth(){
        return this.width;
    }
    public void setWidth(double width){
        this.width = width;
    }
    
    public Rectangle(){
        this.height = 0;
        this.width = 0;
    }
    
    public Rectangle(double height, double width){
        this.height = height;
        this.width = width;
    }

    @Override
    public double getPerimeter() {
        return (2*this.height + 2*this.width);
    }

    @Override
    public double getArea() {
        return (this.height * this.width);
    }
    
    @Override
    public String toString(){
        return (super.toString() + "," + this.height + "," + this.width);
    }
    
    public static Rectangle parse(String input){
        String[] inputs = input.split(",");
        return new Rectangle(Double.parseDouble(inputs[1]), Double.parseDouble(inputs[2]));
    }
}
