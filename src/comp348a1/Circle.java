/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comp348a1;

/**
 *
 * @author Jasmar
 */
public class Circle extends PrintableObject implements Shape{
    
    private double radius;
    
    public double getRadius(){
        return this.radius;
    }
    public void setRadius(double radius){
        this.radius = radius;
    }
    
    public Circle(){
        this.radius = 0;
    }
    
    public Circle(double r){
        this.radius = r;
    }
    
    @Override
    public double getPerimeter() {
        return (2*Math.PI*this.radius);
    }

    @Override
    public double getArea() {
        return (Math.PI*Math.pow(this.radius, 2));
    }
    
    @Override
    public String toString(){
        return (super.toString() + "," + this.radius);
    }
    
    public static Circle parse(String input){
        String[] inputs = input.split(",");
        return new Circle(Double.parseDouble(inputs[1]));
    }
    
    @Override
    public String getName(){
        return super.getName().toUpperCase();
    }
}
