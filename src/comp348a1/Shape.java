/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comp348a1;

/**
 *
 * @author Jasmar
 */
public interface Shape extends NamedObject{
    double getPerimeter();
    double getArea();
}
