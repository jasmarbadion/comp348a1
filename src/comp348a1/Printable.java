/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comp348a1;

/**
 *
 * @author Jasmar
 */
public interface Printable {
    void print();
    
    static void print(Printable... printables){
        for (Printable p : printables){
            p.print();
        }
    }
}
