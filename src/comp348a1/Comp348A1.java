/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comp348a1;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Jasmar
 */
public class Comp348A1 {

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream("C:\\Users\\jasma\\OneDrive\\Documents\\Concordia-Fall 2021\\COMP 348\\Comp348A1\\src\\comp348a1\\shapes.txt"),
                        StandardCharsets.UTF_8)
        )) {
            String line;
            List<Shape> shapes = new ArrayList<>();
            while ((line = br.readLine()) != null) {
                String[] shapeLines = line.split(",");
                shapes.add(
                        (shapeLines[0].equals("Rectangle")) ? 
                                new Rectangle(Double.parseDouble(shapeLines[1]),
                                        Double.parseDouble(shapeLines[2])) : 
                                new Circle(Double.parseDouble(shapeLines[1]))
                );
                Collections.sort(shapes, new Comparator<Shape>() {
                    @Override
                    public int compare(Shape shape1, Shape shape2) {
                           return Double.compare(shape1.getArea(), 
                                   shape2.getArea());
                    }
                });
            }
            System.out.println(shapes);
        }
    }

}
